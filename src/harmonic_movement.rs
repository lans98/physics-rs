pub struct PendulumData {
    pub time: f64,
    pub angle: f64,
    pub speed: f64
}

pub struct SpringData {
    pub time: f64,
    pub position: f64,
    pub speed: f64
}

impl Clone for PendulumData {
    fn clone(&self) -> Self {
        Self {
            time: self.time,
            angle: self.angle,
            speed: self.speed
        }
    }
}

impl Clone for SpringData {
    fn clone(&self) -> Self {
        Self {
            time: self.time,
            position: self.position,
            speed: self.speed
        }
    }
}

pub mod ideal {
    use harmonic_movement::{PendulumData, SpringData};
    use math::euler_method;

    pub fn pendulum(init_speed: f64, init_angle: f64, length: f64, gravity: f64, steps: usize, delta: f64) -> Vec<PendulumData> {
        let mut all_data = Vec::new();
        let mut data     = PendulumData {
            time: 0.0,
            angle: init_angle,
            speed: init_speed
        };

        let mut acceleration: f64;

        for _ in 0usize..steps {
            all_data.push(data.clone());
            acceleration = -gravity * data.angle / length;
            data.angle = euler_method(data.angle, data.speed, delta);
            data.speed = euler_method(data.speed, acceleration, delta);

            data.time += delta;
        }

        all_data
    }

    pub fn spring(mass: f64, spring_k: f64, init_pos: f64, init_speed: f64, steps: usize, delta: f64) -> Vec<SpringData> {
        let mut all_data = Vec::new();
        let mut data     = SpringData {
            time: 0.0,
            position: init_pos,
            speed: init_speed
        };

        let mut acceleration: f64;

        for _ in 0usize..steps {
            all_data.push(data.clone());

            acceleration  = (-spring_k * data.position) / mass;
            data.position = euler_method(data.position, data.speed, delta);
            data.speed    = euler_method(data.speed, acceleration, delta);

            data.time += delta;
        }

        all_data
    }

}

pub mod real {
    use harmonic_movement::{PendulumData, SpringData};
    use math::euler_method;
    use results::ResultStr;

    pub fn pendulum(init_speed: f64, init_angle: f64, length: f64, gravity: f64, k: f64 , steps: usize, delta: f64) -> Vec<PendulumData> {

        let mut all_data = Vec::new();
        let mut data     = PendulumData {
            time: 0.0,
            angle: init_angle,
            speed: init_speed
        };

        let mut acceleration: f64;

        for _ in 0usize..steps {
            all_data.push(data.clone());
            acceleration = -gravity * data.angle / length - k * data.speed;
            data.angle = euler_method(data.angle, data.speed, delta);
            data.speed = euler_method(data.speed, acceleration, delta);

            data.time += delta;
        }

        all_data
    }

    pub fn spring(mass: f64, spring_k: f64, init_pos: f64, init_speed: f64, b: f64, steps: usize, delta: f64) -> ResultStr<Vec<SpringData>> {
        let mut all_data = Vec::new();
        let mut data     = SpringData {
            time: 0.0,
            position: init_pos,
            speed: init_speed
        };

        let mut acceleration: f64;

        if b < 0.0 || b > 1.0 {
          return Err("b should be between 0 and 1 (inclusive)");
        }

        for _ in 0usize..steps {
            all_data.push(data.clone());

            acceleration  = (-spring_k * data.position - b * data.speed) / mass;
            data.position = euler_method(data.position, data.speed, delta);
            data.speed    = euler_method(data.speed, acceleration, delta);

            data.time += delta;
        }

        Ok(all_data)
    }
}
